<?php
	$no=$_GET[no];
	$cid=$_GET[cid];
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <script src="../js/ie-emulation-modes-warning.js"></script>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link href="../css/carousel.css" rel="stylesheet">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

<link rel="stylesheet" href="../css/board.css">
</head>
<body>
<?php include('./nav.html');?>
<h2 style="text-align:center">비밀번호 확인하기</h2>
<form method="post" action="confirm_bdpw_action.php">
<table class="table table-bordered" id="wrap">
	<tr>
		<th>비밀번호 </th>
                <td>
                        <input type="password" class="form-control" placeholder="글 비밀번호를 입력하세요" name="bd_pw">
                </td>
        </tr>
	<tr>
		<td colspan="2" style="text-align:right">
			<input type="hidden" name="no" value=<?=$no?>>
			<input type="hidden" name="cid" value=<?=$cid?>>
			<button type="submit" class="btn btn-default">입력</button>
		</td>
	</tr>
</table>
</form>
<?php include('./footer.html');?>
</body>
</html>
