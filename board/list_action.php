<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <script src="../js/ie-emulation-modes-warning.js"></script>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link href="../css/carousel.css" rel="stylesheet">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

<link rel="stylesheet" href="../css/board.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
	define("PAGE_LIMIT",4);
	include('../util/DBManager.php');
	$dbm=new DBManager();
	$ok=$dbm->getConnect();
	if($ok){
		$page=($_GET['page'])?$_GET['page']:1; //현재 페이지
                $sql="select no from board order by no desc";
                $ret=mysql_query($sql);
                $count=mysql_num_rows($ret); //총 게시글 수
                $block=5; //3.한 블록 당 페이지 수(block)
                $max_pg=ceil($count/PAGE_LIMIT); //총 페이지 수
                $max_block=ceil($max_pg/$block); //총 블록 수
                $now_block=ceil($page/$block); //현재 페이지가 위치한 블록

                $s_page=($now_block*$block)-($block-1);
                if($s_page<=1){
                        $s_page=1;
                }
                $e_page=$now_block*$block;
                if($max_pg<=$e_page){
                        $e_page=$max_pg;
                }

                $pg_offset=($page-1)*PAGE_LIMIT;
                $sql2="select * from board order by no desc limit " . PAGE_LIMIT . " offset $pg_offset";
                $ret2=mysql_query($sql2);
	}
?>
</head>
<body>
<?php include('./nav.html');?>
<h2 style="text-align:center">게시글 리스트</h2>
<table class="table table-bordered" id="wrap">
        <tr>
                <td colspan="6" style="text-align:right">
			page NO. <?= $page?> &nbsp;&nbsp;
                        <a href="write.php">글 작성하기</a>
                </td>
        </tr>

        <tr>
                <th width="5%">번호</th>
                <th>제목</th>
                <th width="20%">글쓴이</th>
                <th width="7%">좋아요</th>
                <th width="7%">싫어요</th>
                <th width="7%">조회수</th>
        </tr>
      
	<?php for($num=0;$num<$count;$num++){ 
		$row=mysql_fetch_array($ret2);
	?>
	<?php
	$goodNum=$row[good];
	$badNum=$row[bad];
	$diff=$goodNum-$badNum;
	?>
	<tr>
                <td width="5%"><?= $row["no"]?></td>
                <td><a 
		<?php 
			if($diff==1){
				echo 'style="color:#737373;font-weight:500"';
			}else if($diff==2){
				echo 'style="color:#666666;font-weight:500"';
			}else if($diff==3){
				echo 'style="color:#595959;font-weight:500"';
			}else if($diff==4){
				echo 'style="color:#4d4d4d;font-weight:600"';
			}else if($diff==5){
				echo 'style="color:#404040;font-weight:600"';
			}else if($diff==6){
				echo 'style="color:#333333;font-weight:600"';
			}else if($diff==7){
				echo 'style="color:#262626;font-weight:700"';
			}else if($diff==8){
				echo 'style="color:#1a1a1a;font-weight:700"';
			}else if($diff==9){
				echo 'style="color:#0d0d0d;font-weight:700"';
			}else if($diff==10){
				echo 'style="color:#000000;font-weight:800"';
			}else if($diff>10){
				echo 'style="color:blue;font-weight:900"';
			}else if($diff==-1){
				echo 'style="color:#8c8c8c"';
			}else if($diff==-2){
				echo 'style="color:#999999"';
			}else if($diff==-3){
				echo 'style="color:#a6a6a6"';
			}else if($diff==-4){
				echo 'style="color:#b3b3b3"';
			}else if($diff==-5){
				echo 'style="color:#bfbfbf"';
			}else if($diff==-6){
				echo 'style="color:#cccccc"';
			}else if($diff==-7){
				echo 'style="color:#d9d9d9"';
			}else if($diff==-8){
				echo 'style="color:#e6e6e6"';
			}else if($diff==-9){
				echo 'style="color:#f2f2f2"';
			}else if($diff==-10){
				echo 'style="color:#ffffff"';
			}else if($diff<-10){
				echo 'style="color:red"';
			}
		?>
		id="title" href="view_action.php?no=<?= $row[no]?>"><?= $row["title"]?>
			</a>
		</td>
                <td><?= $row["author"]?></td>
                <td><?= $row["good"]?></td>
                <td><?= $row["bad"]?></td>
                <td><?= $row["count"]?></td>
        </tr>
        <?php 
		if($row==false){
			break;
		}
	} //반복문 끝?>
	<tr>
		<td colspan="6" style="text-align:center;">
			<nav aria-label="Page navigation">
  				<ul class="pagination">
    					<li>
      						<a href="list_action.php?page=<?=$s_page-1?>" aria-label="Previous">
        					<span aria-hidden="true">&laquo;</span>
      						</a>
    					</li>
					<?php for($p=$s_page;$p<=$e_page;$p++){?>
						<li><a href="list_action.php?page=<?=$p?>"><?=$p?></a></li>
					<?php }?>
    					<li>
      						<a href="list_action.php?page=<?=$e_page+1?>" aria-label="Next">
        					<span aria-hidden="true">&raquo;</span>
      						</a>
    					</li>
  				</ul>
			</nav>

		</td>
	</tr>
</table>
	<?php include('./footer.html');?>
</body>
</html>

