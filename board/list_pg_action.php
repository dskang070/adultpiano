<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="../css/board.css">
<?php
	define("PAGE_LIMIT",4); //1.페이지 당 게시글 수(list)
        include('../util/DBManager.php');
        $dbm=new DBManager();
        $ok=$dbm->getConnect();
        if($ok){
		$page=($_GET['page'])?$_GET['page']:1; //현재 페이지
                $sql="select no from board order by no desc";
                $ret=mysql_query($sql);
		$count=mysql_num_rows($ret); //2.총 게시글 수(data)
		$block=5; //3.한 블록 당 페이지 수(block)
        	$max_pg=ceil($count/PAGE_LIMIT); //총 페이지 수(pageNum)
		$max_block=ceil($max_pg/$block); //총 블록 수(blockNum)
		$now_block=ceil($page/$block); //현재 페이지가 위치한 블록
		
		$s_page=($now_block*$block)-($block-1);
		if($s_page<=1){
			$s_page=1;
		}
		$e_page=$now_block*$block;
		if($max_pg<=$e_page){
			$e_page=$max_pg;
		}

		echo "현재 페이지는".$page."<br/>";
		echo "현재 블록은".$now_block."<br/>";

		echo "현재 블록의 시작 페이지는".$s_page."<br/>";
		echo "현재 블록의 끝 페이지는".$e_page."<br/>";

		echo "총 페이지는".$max_pg."<br/>";
		echo "총 블록은".$max_block."<br/>";	

		for($p=$s_page;$p<=$e_page;$p++){ ?>
			<a href="list_pg_action.php?page=<?=$p?>"><?=$p?></a>
		<?php }?>
<?php }?>
</head>
<body>
<div>
	<a href="list_pg_action.php?page=<?=$s_page-1?>">이전</a>
	<a href="list_pg_action.php?page=<?=$e_page+1?>">다음</a>
</div>
<?php
	$pg_offset=($page-1)*PAGE_LIMIT;
	echo PAGE_LIMIT."<br>";
	echo $pg_offset;
	$sql2="select * from board order by no desc limit " . PAGE_LIMIT . " offset $pg_offset";
	$ret2=mysql_query($sql2);
?>


<h2 style="text-align:center">게시글 리스트</h2>
<table class="table table-bordered" id="wrap">
        <tr>
                <td colspan="6" style="text-align:right">
                        <a href="write.php">글 작성하기</a>
                </td>
        </tr>

        <tr>
                <th>번호</th>
                <th>제목</th>
                <th>글쓴이</th>
                <th>좋아요</th>
                <th>싫어요</th>
                <th>조회수</th>
        </tr>

        <?php for($num=0;$num<$count;$num++){
		$hello=mysql_num_rows($ret2);
		echo "hello:".$hello; 
		$row=mysql_fetch_array($ret2);
	?>
        <tr>
                <td><?= $row["no"]?></td>
                <td><a href="view_action.php?no=<?= $row[no]?>"><?= $row["title"]?></a></td>
                <td><?= $row["author"]?></td>
                <td><?= $row["good"]?></td>
                <td><?= $row["bad"]?></td>
                <td><?= $row["count"]?></td>
        </tr>
        <?php 
		if($row==false){
			break;
		}
	} ?>
	
<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>

</table>
</body>
</html>


