<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <script src="../js/ie-emulation-modes-warning.js"></script>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link href="../css/carousel.css" rel="stylesheet">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

<link rel="stylesheet" href="../css/board.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>
<?php
        include('../util/DBManager.php');

        $dbm=new DBManager();
        $ok=$dbm->getConnect();
        if(!$ok){
                echo "database error...";
        }else{
                $sql="select * from board where no='$_GET[no]'";
                $ret=mysql_query($sql);
                $row=mysql_fetch_array($ret);
        }
?>

</head>
<body>
<?php include('./nav.html');?>
<h2 style="text-align:center">게시글 수정하기</h2>
<form method="post" action="update_action.php" enctype="multipart/form-data">
<table class="table table-bordered" id="wrap">
        <tr>
                <th>제목</th>
                <td>
                        <input type="text" class="form-control" placeholder="Text input" name="title" value="<?= $row[title] ?>" >
                </td>
        </tr>

        <tr>
            	<th>글쓴이</th>
                <td>
                        <input type="text" class="form-control" placeholder="Text input" name="author" value="<?= $row[author] ?>" readonly>
                </td>
        </tr>

        <tr>
                <th>내용</th>
                <td>
                        <textarea class="form-control" cols="40" rows="10" name="body">
                                <?= $row[body]?>
                        </textarea>
                </td>
        </tr>
        <tr>
                <th>파일첨부</th>
                <td>
                	<input type="file" class="form-control" name="userfile" required>
		</td>
        </tr>
        <tr>
                <td colspan="2" style="text-align:right">
                        <input type="hidden" name="no" value="<?= $row[no]?>">
                        <button type="submit" class="btn btn-default">수정완료</button>
                        <button type="button" class="btn btn-default" onclick="location='list_action.php';">목록으로</button>
                </td>
        </tr>
</table>
</form>
<?php include('./footer.html');?>
</body>
</html>
