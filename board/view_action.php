<html>
<head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <script src="../js/ie-emulation-modes-warning.js"></script>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link href="../css/carousel.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/board.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

</head>
<?php
        include('../util/DBManager.php');
        $dbm=new DBManager();
        $ok=$dbm->getConnect();
	$no=$_GET[no];
        if(!isset($ok)){
                echo "database error...";
        }else{
		$sql2="update board set count=(count+1) where no='$no'";
		mysql_query($sql2);
                $sql="select * from board where no='$no'";
                $ret=mysql_query($sql);
                $row=mysql_fetch_array($ret);

		$sheet_no=$row[sheet_no];
		$sql3="select my_sheet from sheet_music where no='$sheet_no'";
		$ret2=mysql_query($sql3);
		$row2=mysql_fetch_array($ret2);
		$my_sheet=$row2[my_sheet];
		$dir="../my_sheet_music/";
		$my_sheet=$dir.$my_sheet;
        }
?>
<script>
	function confirm(cid){
		location.replace("confirm_bdpw.php?no=<?=$no?>&cid="+cid);
	}
	
	function good_bad(cid){
		location.replace("good_bad.php?no=<?=$no?>&cid="+cid);
	}
</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<?php include('./nav.html');?>

<h2 style="text-align:center">게시글 보기</h2>
<table class="table table-bordered" id="wrap">
        <tr>
                <th>제목</th>
                <td>
                        <input type="text" class="form-control" placeholder="Text input" name="title" value="<?= $row[title] ?>" readonly>
                </td>
        </tr>

        <tr>
            	<th>글쓴이</th>
                <td>
                        <input type="text" class="form-control" placeholder="Text input" name="author" value="<?= $row[author] ?>" readonly>
                </td>
        </tr>

        <tr>
                <th>나의악보</th>
                <td>
                <?php
		if(!empty($row2[my_sheet])){
			header('Content-Type image/png');
			echo "<img src='$my_sheet'/>";
			echo "   <a href='../piano/play3.php?sheet_no=".$row[sheet_no]."'>연주하러 가기</a>";
		}else{
			echo "공유된 나의 악보가 없습니다. ";
		}
		?>
		</td>
        </tr>
        <tr>
            	<th>내용</th>
                <td>
                	<textarea class="form-control" cols="40" rows="10" name="body" readonly>
                                <?= $row[body]?>
                        </textarea>
		</td>
        </tr>
        <tr>
                <th>첨부파일(download)</th>
                <td>
		<?php if(isset($row[userfile])){ ?>
			<a href="download.php?no=<?= $row[no]?>">
				<?= $row[userfile]?>
			</a>
		<?php }else 
			echo "첨부된 파일이 없습니다.";
		?>
                </td>
        </tr>
        <tr>
                <td colspan="2" style="text-align:right">
                        <button id="good" type="button" class="btn btn-primary" onclick="good_bad(this.id);">좋아요</button>
                        <button id="bad" type="button" class="btn btn-danger" onclick="good_bad(this.id);">싫어요</button>
                        <button id="u" type="button" class="btn btn-default" onclick="confirm(this.id);">수정하기</button>
                        <button id="d" type="button" class="btn btn-default" onclick="confirm(this.id);">삭제하기</button>
                        <button type="button" class="btn btn-default" onclick="location='list_action.php';">목록으로</button>
                </td>
        </tr>
</table>

<?php include('./footer.html');?>
</body>
</html>
