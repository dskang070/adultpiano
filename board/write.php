<?php session_start();?>
<?php
	$sid=$_SESSION["ses_userid"];
	include('../util/DBManager.php');
        $dbm=new DBManager();
        $dbm->getConnect();
?>
<?php if($sid!="guest") { ?>
<?php
	$sheet_no=$_GET["sheet_no"];
	$sql="select my_sheet from sheet_music where no='$sheet_no'";
	$ret=mysql_query($sql);
	$row=mysql_fetch_array($ret);
	$dir="../my_sheet_music/";
	$my_sheet=$dir.$row["my_sheet"];
?>

<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
        <script src="../js/ie-emulation-modes-warning.js"></script>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link href="../css/carousel.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/board.css">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

</head>
<body>
<?php include('./nav.html');?>
<h2 style="text-align:center">게시글 작성하기</h2>
<form method="post" action="write_action.php" enctype="multipart/form-data">
<table class="table table-bordered" id="wrap">
<tr>
	<th>제목</th>
	<td>
		<input type="text" class="form-control" placeholder="제목을 입력하세요" name="title" required>
	</td>
</tr>

<tr>
	<th>글쓴이</th>
	<td>
		<input type="text" class="form-control" name="author" value=<?=$sid?> readonly>
	</td>
</tr>

<tr>
	<th>비밀번호</th>
	<td>
		<input type="password" class="form-control" placeholder="글 비밀번호를 입력하세요" name="bd_pw" required>
	</td>
</tr>

<tr>
	<th>나의악보</th>
		<td>
		<?php
                if(!empty($row)){
                        header('Content-Type image/png');
                        echo "<img src='$my_sheet'/>";
                }else{
                        echo "이미지 없음 ";
                }
                ?>
		</td>
        </tr>
<tr>
	<th>내용</th>
		<td>
			 <textarea class="form-control" cols="40" rows="10" name="body" required>
                        </textarea>			
		</td>
</tr>
        <tr>
            	<th>첨부파일</th>
                <td>
                        <input type="file" class="form-control" name="userfile" accept="image/png">
                </td>
        </tr>
        <tr>
                <td colspan="2" style="text-align:right">
			<input type="hidden" name="sheet_no" value=<?=$sheet_no?>>	
                        <button type="submit" class="btn btn-default">작성하기</button>
                        <button type="button" class="btn btn-default" onclick="location='list_action.php';">목록으로</button>
                </td>
        </tr>
</table>
</form>
<?php include('./footer.html');?>
</body>
</html>
<?php }else{ ?>
	<script>alert('회원만 음악공유가 가능합니다!');</script>
	<script>location.replace('./list_action.php');</script>
<?php }?>
