<?php session_start();?>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>AdultPiano!!!</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js" ></script>
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="./css/signin.css" rel="stylesheet">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="./piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

    <script type="text/javascript" src="./js/mySignInForm.js"></script>
</head>
<body>
    <div class="container">
<?php if(!isset($_SESSION[ses_userid])||($_SESSION[ses_userid]=="guest")){ ?>
     <h2 class="form-signin-heading" style="text-align:center;font-weight:800;">로그인</h2>
        <form class="form-signin" name="singIn" action="./member/signIn.php" method="post" onsubmit="return checkSubmit()">
            <div class="form-group has-error">
                <input type="text" name="memberId"  class="form-control" placeholder="아이디" required autofocus>
            </div>
            <div class="form-group has-error">
                <input type="password" name="memberPw" class="form-control" placeholder="비밀번호" required>
            </div>
	    <div class="form-group has-error">
                &nbsp;
                <a href="./member/signUpForm.php" style="color:black;">아직 회원이 아니세요?</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="./member/guest.php" style="color:black;">손님으로 입장하기</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		
            </div>
	    <button class="btn btn-lg btn-danger btn-block" type="submit">입장하기</button>
        </form>
    </div>
<?php }else{
	echo "<script>location.replace('./view.php');</script>";
}?>
</body>
</html>
