<?php
    include "../util/dbConnect.php";
     
    $memberId = $_POST['memberId'];
    $memberName = $_POST['memberName'];
    $memberPw = $_POST['memberPw'];
    $memberPw2 = $_POST['memberPw2'];
    $memberNickName = $_POST['memberNickName'];
    $email1 = $_POST['email1'];
    $email2 = $_POST['email2'];
    $emailAddress = $email1.'@'.$email2;
    $memberBirthDay = $_POST['memberBirthDay'];
 
    //PHP에서 유효성 재확인
    
    //19세 미만 가입불가
    $birthday_byNum=(int)$memberBirthDay;//930608
    $birthday_calc=floor($birthday_byNum/10000);
    if($birthday_calc<98 && $birthday_calc>16){
    
    //이름에 공백 허용안됨
    if(!ereg("([^[:space:]]+)",$memberName) || ereg("([[:space:]]+)",$memberName)) {
	echo "<script> alert('이름에 공백이 존재합니다!!'); </script>";
       	echo "<script>history.back();</script>";
       	exit;
    }

    //이름 한글만 허용
    for($i=0;$i<strlen($memberName);$i++) {
	if(ord($memberName[$i]) <= 0x80){
		echo "<script> alert('이름은 한글로만 써주세요!!'); </script>";
       		echo "<script>history.back();</script>";
       		exit;
	}
    }

    //아이디 글자수 
    if(!ereg("[[:alnum:]+]{5,20}",$memberId)) {
	echo "<script> alert('ID는 최소 5글자 이상 되어야합니다!!'); </script>";
        echo "<script>history.back();</script>";
        exit;
    }

    //아이디 중복검사.
    $sql = "SELECT * FROM member WHERE memberId = '$memberId'";
    $res = $dbConnect->query($sql);
    if($res->num_rows >= 1){
        echo "<script>alert('이미 존재하는 아이디가 있습니다');</script>";
        echo "<script>history.back();</script>";
        exit;
    }

    //닉네임 중복검사
    $sql = "SELECT * FROM member WHERE nickname = '$memberNickName'";
    $res = $dbConnect->query($sql);
    if($res->num_rows >= 1){
        echo "<script>alert('이미 존재하는 닉네임 있습니다!!');</script>";
        echo "<script>history.back();</script>";
        exit;
    }
		
    //패스워드 정규화
    if(!preg_match('/^[0-9a-zA-Z~!@#$%^&*]{8,15}$/',$memberPw) || !preg_match('/\d/',$memberPw) || !preg_match('/[a-zA-Z]/',$memberPw)){
	echo "<script> alert('8~15 글자의 숫자/영어/특수문자를  혼합해서 사용하세요!!');  </script>";
	echo "<script> history.back(); </script>";
	exit;
    }

    //패스워드 일치하는지 확인
    if($memberPw !== $memberPw2){
        echo "<script>alert('비밀번호가 일치하지 않습니다.');</script>";
        echo "<script> history.back();</script>";
	exit;
    }else{
        //패스워드를 암호화 처리.
        $memberPw = md5($memberPw);
    }
 
    //닉네임, 생일 그리고 이름이 빈값이 아닌지
    if($memberNickName == '' || $memberBirthDay == '' || $memberName == ''){
        echo "<script>alert('생일혹은 닉네임의 값이 없습니다');</script>";
        echo "<script>history.back();</script>";
	exit;
    }
 
    //이메일 주소가 올바른지
    $checkEmailAddress = filter_var($emailAddress, FILTER_VALIDATE_EMAIL);
    if($checkEmailAddress != true){
        echo "<script>alert('올바른 이메일 주소가 아닙니다');</script>";
        echo "<script>history.back();</script>";
        exit;
    }
 }else{
	echo "<script> alert('애들은 가라...'); </script>";
	echo "<script> history.back();</script>";
	exit;
    }


 
    //이제부터 넣기 시작
    $sql = "INSERT INTO member VALUES('$memberId','$memberName','$memberNickName','$memberPw','$emailAddress','$memberBirthDay')";
 
    if($dbConnect->query($sql)){
        echo "<script>alert('회원가입에 성공하였습니다');</script>";
	echo "<meta http-equiv='refresh' content='0; url=../main.php'>";
    }
?> 
