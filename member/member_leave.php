<?php session_start();?>
<?php
	$sid=$_SESSION['ses_userid'];
	if(!$sid){
		echo "<script>alert('로그인 정보가 없는 상태입니다');</script>";
                echo "<script>history.back();</script>";
	
	}
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js" ></script>
<script type="text/javascript" src="../js/mySignupForm.js"></script>
<link rel="stylesheet" href="../css/mySignupForm.css" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<link href="../css/signin.css" rel="stylesheet">
<link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>


</head>
<body>
	<div class="container">
	<h2 class="form-signin-heading" style="text-align:center;font-weight:800">ADULT PIANO's WITHDRAWL</h2>
	<form class="form-signin" action="./member_leave_chk.php" method="post">
		<div class="form-signin form-group has-error">
                <label>비밀번호</label>
                <input type="password" name="memberPw" class="form-control" required>
            	</div>
		<button class="form-signin btn btn-lg btn-danger btn-block" type="submit">V 탈퇴하기</button>
        <button class="form-signin btn btn-lg btn-danger btn-block" type="reset">다시입력</button>
        <button class="form-signin btn btn-lg btn-danger btn-block" onclick="location.replace('../view.php');">홈으로</button>
		

	</form>
	</div>
</body>
</html>
