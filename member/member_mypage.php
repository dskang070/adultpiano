<?php session_start();?>
<?php
//회원 자신의 페이지를 조회하는 페이지이다.
//회원의 정보와 회원이 쓴 글 그리고  그 글의 좋아요,싫어요 갯수가 같이 표시된다.
//수정하기, 탈퇴하기 버튼이 있다.
	$sid=$_SESSION["ses_userid"];
	include('../util/DBManager.php');
	$dbm=new DBManager();
	$dbm->getConnect();
	$sql="select memberId,name,nickname,email,birthDay from member where memberId='$sid'";
	$ret=mysql_query($sql);
	$row=mysql_fetch_array($ret);
	$sql2="select * from board where author='$sid'";
	$ret2=mysql_query($sql2);
	$count=mysql_num_rows($ret2);
	$row2=mysql_fetch_array($ret2);
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js" ></script>
<script type="text/javascript" src="../js/mySignupForm.js"></script>
<link rel="stylesheet" href="../css/mySignupForm.css" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<link href="../css/signin.css" rel="stylesheet">

<script>
	function member_mo(){
		if(!document.member_mypage.memberPw.value){
			alert('정보를 수정하려면 비밀번호를 입력하세요');
			document.member_mypage.memberPw.focus();
		}else{
			document.member_mypage.action="./member_modify_pwchk.php";
			document.member_mypage.gogo.value = "1";
			document.member_mypage.submit();
		}
	}

	function member_le(){
		if(!document.member_mypage.memberPw.value){
			alert('탈퇴 하시려면 비밀번호를 입력하세요');
			document.member_mypage.memberPw.focus();
		}else{
			document.member_mypage.action="./member_modify_pwchk.php";
			document.member_mypage.gogo.value = "2";
			document.member_mypage.submit();
		}
	}


</script>
</head>
<body>
<div oncontextmenu="return false">
    <div class="container">
	<h2 class="form-signin-heading" style="text-align:center;font-weight:800">ADULT PIANO's MY PAGE</h2>
            <div class="form-signin form-group has-error">
		<label>아이디</label>
                <input type="text" name="memberId" class="memberId form-control" value="<?= $row[memberId]?>" readonly>
	    </div>
            <div class="form-signin form-group has-error">
		<label>이름</label>
                    <input type="text" name="memberName" class="memberName form-control" value="<?=$row[name]?>" readonly>
            </div>
            <div class="form-signin form-group has-error">
		<label>닉네임</label>
                    <input type="text" name="memberNickName" class="memberNickName form-control" value="<?=$row[nickname]?>" readonly>
	    </div> 
            <div class="form-signin form-group has-error">
		<label>email</label>
                    <input type="text" name="email1" class="emailAddress form-control" value="<?=$row[email]?>" readonly>
            </div>
            <div class="form-signin form-group has-error">
		<label>생년월일</label>
                    <input type="text" name="memberBirthDay" class="memberBirthDay form-control"  value="<?=$row[birthDay]?>" readonly>
            </div>
            <div class="form-signin form-group has-error">
		<label>나의 글</label>
                    <input type="text" name="memberBirthDay" class="memberBirthDay form-control"  value="총 <?=$count?>개의 글" readonly>
            </div>
	<form method="post" name="member_mypage">
            <div class="form-signin form-group has-error">
		<label>정보를 수정하거나 탈퇴하시려면 비밀번호를 입력해 주세요</label>
                    <input type="password" name="memberPw" class="form-control">
                    <input type="hidden" name="gogo" class="form-control">
            </div>
        <button class="form-signin btn btn-lg btn-danger btn-block" onclick="member_mo();">내 정보 수정하기</button>
        <button class="form-signin btn btn-lg btn-danger btn-block" onclick="member_le();">탈퇴하기</button>
	</form>
        <button class="form-signin btn btn-lg btn-danger btn-block" onclick="location.replace('../view.php');">홈으로</button>
    </div>
</div>
</body>
</html>
