<?php session_start(); ?>
<?php
	//회원정보를 수정하는 페이지.
	//view페이지와 똑같이 구성하되, 비밀번호 체크하는 것만 추가.

	include('../util/DBManager.php');
	$dbm=new DBManager();
	$dbm->getConnect();


	$sid=$_SESSION['ses_userid'];
	if(!$sid){
                echo "<script>alert('로그인 정보가 없는 상태입니다');</script>";
                echo "<script>history.back();</script>";
	}
	$sql="select * from member where memberId='$sid'";
	$ret=mysql_query($sql);
	$row=mysql_fetch_array($ret);
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js" ></script>
<script type="text/javascript" src="../js/mySignupForm.js"></script>
<link rel="stylesheet" href="../css/mySignupForm.css" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<link href="../css/signin.css" rel="stylesheet">
<link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

<script>

function modifyinfocheck(){
		if(!document.modifyinfoform.memberPw.value){
			alert('비밀번호를 입력하세요');
			document.modifyinfoform.memberPw.focus();
			return false;
		}else if(!document.modifyinfoform.memberPw2.value){
			document.modifyinfoform.memberPw2.value = document.modifyinfoform.memberPw.value;
			document.modifyinfoform.memberPw3.value = document.modifyinfoform.memberPw.value;
		}else if(document.modifyinfoform.memberPw2.value.match('/^[0-9a-zA-Z~!@#$%^&*]{8,15}/') != null || document.modifyinfoform.memberPw2.value.length<7){
			alert('새로 만드는 비밀번호는 공백없이 8자리 이상이어야 합니다.');
			document.modifyinfoform.memberPw2.focus();
			return false;
		}else if(document.modifyinfoform.memberPw2.value != document.modifyinfoform.memberPw3.value){
			alert('변경할 비밀번호가 일치하지 않습니다');
			document.modifyinfoform.memberPw3.value="";
			document.modifyinfoform.memberPw3.focus();
			return false;
		}
		return true;
}	

</script>
</head>
<body>
<div oncontextmenu="return false">
    <div class="container">
	<h2 class="form-signin-heading" style="text-align:center;font-weight:800">ADULT PIANO's UPDATE MY DATA</h2>
	<form class="form-signin" action="./member_update_chk.php" method="post" onsubmit="return modifyinfocheck();" name="modifyinfoform">
            <div class="form-signin form-group has-error">
		<label>아이디</label>
                <input type="text" name="memberId" class="memberId form-control" value="<?= $row[memberId]?>" required>
	    </div>
            <div class="form-signin form-group has-error">
		<label>현재 비밀번호</label>
                <input type="password" name="memberPw" class="form-control">
	    </div>
            <div class="form-signin form-group has-error">
		<label>변경할 비밀번호</label>
                <input type="password" name="memberPw2" class="form-control">
	    </div>
            <div class="form-signin form-group has-error">
		<label>변경할 비밀번호 확인</label>
                <input type="password" name="memberPw3" class="form-control">
	    </div>
            <div class="form-signin form-group has-error">
		<label>닉네임</label>
                    <input type="text" name="memberNickName" class="memberNickName form-control" value="<?=$row[nickname]?>" required>
	    </div> 
            <div class="form-signin form-group has-error">
		<label>생년월일</label>
                    <input type="text" name="memberBirthDay" class="memberBirthDay form-control"  value="<?=$row[birthDay]?>" required>
            </div>
        <button class="form-signin btn btn-lg btn-danger btn-block" type="submit">V 수정완료</button>
        <button class="form-signin btn btn-lg btn-danger btn-block" type="reset">다시입력</button>
	</form>
        <button class="form-signin btn btn-lg btn-danger btn-block" onclick="location.replace('../view.php');">홈으로</button>
    </div>
</div>
</body>
</html>

