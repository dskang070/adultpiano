<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<title> join AdultPiano</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js" ></script>
<script type="text/javascript" src="../js/mySignupForm.js"></script>
<link rel="stylesheet" href="../css/mySignupForm.css" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<link href="../css/signin.css" rel="stylesheet">
<link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>


</head>
<body>
<div oncontextmenu="return false">
    <div class="container">
	<h2 class="form-signin-heading" style="text-align:center;font-weight:800">ADULT PIANO</h2>
        <form name="signUp" action="./memberSave.php" method="post" onsubmit="return checkSubmit()" class="form-signin">
            <div class="form-group has-error">
                <input type="text" name="memberId" class="memberId form-control" placeholder="아이디(최소 5글자 이어야 합니다)" required autofocus>
	    </div>
 	    <div class="form-group has-error">
		<button type="button" class="memberIdCheck btn btn-danger btn-xs">아이디 중복확인</button>
                <div class="memberIdComment comment"></div>
            </div>
            <div class="form-group has-error">
                    <input type="text" name="memberName" class="memberName form-control" placeholder="이름" required autofocus>
            </div>
            <div class="form-group has-error">
                    <input type="password" name="memberPw" class="memberPw form-control" placeholder="비밀번호(8~15글자의 영문/숫자/특수문자만 가능합니다)" required autofocus>
            </div>
            <div class="form-group has-error">
                    <input type="password" name="memberPw2" class="memberPw2 form-control" placeholder="비밀번호 확인" required autofocus>
                    <div class="memberPw2Comment comment"></div>
            </div>
            <div class="form-group has-error">
                    <input type="text" name="memberNickName" class="memberNickName form-control" placeholder="닉네임" required autofocus">
	    </div> 
	    <div class="form-group has-error">
		    <button type="button" class="memberNickNameCheck btn btn-danger btn-xs">닉네임 중복확인</button>
                    <div class="memberNickNameComment comment"></div>
            </div>
            <div class="form-group has-error">
                    <input type="text" name="email1" class="emailAddress form-control" style="ime-mmde:inactive" placeholder="이메일 아이디">@<select name="email2" class="emailAddress2 form-control">
			<option value="naver.com"> naver.com </option>
			<option value="nate.com"> nate.com </option>
			<option value="daum.net"> daum.net </option>
			<option value="google.com"> google.com </option>
			<option value="gmail.com"> gmail.com </option>
			<option value="korea.com"> korea.com </option>
			<option value="hotmail.com"> hotmail.com </option>
			<option value="dreamwiz.com"> dreamwiz.com </option>
			</select>
                    <div class="emailAddressComment comment"></div>
            </div>
            <div class="form-group has-error">
                    <input type="text" name="memberBirthDay" class="memberBirthDay form-control"  placeholder="생년월일 ex)930608" required>
                    <div class="memberBirthDayComment comment"></div>
            </div>
	    <div class="form-group has-error">
            <a href="../view.php" style="color:black;">손님으로 입장하기</a>
            </div>
        <button class="btn btn-lg btn-danger btn-block" type="submit">V 가입하기</button>
        <button class="btn btn-lg btn-danger btn-block" type="reset">X 취    소</button>
        </form>
        <button class="btn btn-lg btn-danger btn-block" onclick="location.replace('../main.php');">로그인화면으로가기</button>
        <div class="formCheck">
            <input type="hidden" name="memberId" class="idCheck" value="memberId">
            <input type="hidden" name="memberNickName" class="nickCheck" value="memberNickName">
            <input type="hidden" name="pw2Check" class="pwCheck2">
            <input type="hidden" name="eMailCheck" class="eMailCheck">
	</div>
    </div>
</div>
</body>
</html>
