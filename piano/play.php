<html>
<head>
        <link rel="stylesheet" type="text/css" href="play.css">
	<script type="text/javascript" src="./html2canvas.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="play.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
   	<meta name="author" content="">
    	<!-- Bootstrap core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">
    	<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    	<script src="../js/ie-emulation-modes-warning.js"></script>
    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<link href="../css/carousel.css" rel="stylesheet">
<script>
	function share(){
		html2canvas($("#sheet_music"),{
			onrendered: function(canvas){
				var img=canvas.toDataURL("image/png");
				$('#img_val').val(img);
				document.getElementById("myForm").submit();
			}
		});
	}
</script>
</head>
<body>
<form method="post" enctype="multipart/form-data" action="./img_save.php" id="myForm" name="myForm">
	<input type="hidden" name="img" id="img_val" value="" />
</form>
<audio>
    <source src="do.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="re.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="mi.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="fa.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="sol.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="ra.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="si.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="do2.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="do_shap.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="re_shap.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="fa_shap.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="sol_shap.mp3" type="audio/mpeg"></source>
</audio>
<audio>
    <source src="ra_shap.mp3" type="audio/mpeg"></source>
</audio>
<?php session_start();?>
<?php
	$sid=$_SESSION["ses_userid"];
?>


<div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="../view.php" style="font-weight:800;">AdultPiano</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><a href="../view.php">홈</a></li>
                <li class="active"><a href="./play.php">연주하기</a></li>
                <li><a href="../board/list_action.php?page=1">공유하기</a></li>

                 <?php if($sid!=""){ ?>
                <li><a href="../member/signOut.php">로그아웃[<?=$sid?>님]</a></li>
                <?php }else{ ?>
                <li><a href="../member/signUpForm.php">아직 회원이 아니세요?
                <li><a href="../main.html">로그인</a></li>
                <?php } ?>

              </ul>
            </div>
          </div>
        </nav>
      </div>
</div>

<?php include('sheet_music.html');?>

<div id="piano">
         <div id="piano-container">
                    <li>
                        <div id="S" onclick="S()" >
                        <h1 class="text">S</h1>
                        </div>
                        <div style="color:white;text-align:center" onclick="E()"  id="E">
                        <h1>E</h1>
                        </div>
                    </li>
		    <li>
                        <div onclick="D()" id="D">
                        <h1 class="text">D</h1>

                        </div>
                        <div style="color:white;text-align:center" onclick="R()" id="R">
                        <h1>R</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="F()" id="F" onkeypress="S()" >
                        <h1 class="text">F</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="G()" id="G">
                        <h1 class="text">G</h1>

                        </div>
                        <div style="color:white;text-align:center" onclick="Y()" id="Y">
                        <h1>Y</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="H()" id="H">
                        <h1 class="text">H</h1>

                        </div>
                        <div style="color:white;text-align:center" onclick="U()" id="U">
                        <h1>U</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="J()" id="J">
                        <h1 class="text">J</h1>

                        </div>
                        <div style="color:white;text-align:center" onclick="I()" id="I">
                        <h1>I</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="K()" id="K">
                        <h1 class="text">K</h1>

                        </div>
                    </li>
                    <li>
                        <div onclick="L()" id="L">
                        <h1 class="text">L</h1>
                        </div>
                    </li>

         </div>
</div>


<button type="submit" class="btn btn-default" onclick="share();">공유하기</button><br><br>
<button type="button" class="btn btn-default" onclick="location='./sheet_download.php';">악보저장하기(다운로드)</button>


<hr class="featurette-divider">
<footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>&copy; 2016 AdultPiano.com by 강다솔,강한올,김종대,박진표</p>
</footer>


</body>
</html>

