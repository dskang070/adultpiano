<!DOCTYPE html>
<html>
<head>
        <link rel="stylesheet" type="text/css" href="../css/play.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="../js/play.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="../js/jquery.ulslide.js" type="text/javascript" charset="utf-8"></script>
	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
   	<meta name="author" content="">
    	<!-- Bootstrap core CSS -->
    	<link href="../css/bootstrap.min.css" rel="stylesheet">
    	<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    	<script src="../js/ie-emulation-modes-warning.js"></script>
    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<link href="../css/carousel.css" rel="stylesheet">
	 <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="../piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>

	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script type="text/javascript">
            $(function() {
                $('#main_bn').ulslide({
                    statusbar: true,
                    width: 550,
                    height: 400,
                    affect: 'slide',
                    axis: 'x',
                    navigator: '#main_bn_btn a',
                    duration: 400,
                });
            });
</script>
</head>
<body>
<?php include('./sound.html');?>
<?php include('./nav.html');?>
<div id="wrap">
        <div id="main_bn">
                <li>
                    <img src="./img_sample/bi.png" alt="" height="300"/>
                </li>
                <li>
                    <img src="./img_sample/san.png" alt="" height="300" />
                </li>
                <li>
                    <img src="./img_sample/mi.png" alt="" height="300"/>
                </li>
                <li>
                    <img src="./img_sample/se.png" alt="" height="300"/>
                </li>
                <li>
                    <img src="./img_sample/sa.png" alt="" height="300"/>
                </li>
                <li>
                    <img src="./img_sample/gom.png" alt="" height="300"/>
                </li>
                <li>
                    <img src="./img_sample/si.png" alt="" height="300"/>
                </li>
            <div id="main_bn_btn">
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
                <li><a href="#"><img src="./img_sample/blt.png"></a></li>
            </div>
      </div>
</div>
<?php include('./mypiano.html');?>
<?php include('./footer.html');?>
</body>
</html>

