<?php 
	session_start(); 
	$sid=$_SESSION["ses_userid"];
?>
<html>
<lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link href="css/carousel.css" rel="stylesheet">
    <link rel="shortcut icon" href="./piano_icon_Pp4_icon.ico" type="image/x-icon" />
    <title>AdultPiano</title>
  </head>
<!-- 네비게이션 바 -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="./view.php" style="font-weight:800;">AdultPiano</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="./view.php">홈</a></li>
		<li class="dropdown">
        	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">연주하기 <span class="caret"></span></a>
          	  <ul class="dropdown-menu">
            		<li><a href="./piano/play2.php">악보 보며 연주하기</a></li>
            		<li><a href="./piano/play3.php">악보 만들며 연주하기</a></li>
          	  </ul>
        	</li>	


                <li><a href="./board/list_action.php?page=1">공유하기</a></li>
	      </ul>
              <ul class="nav navbar-nav navbar-right">
		<?php if($sid!="guest"){ ?>
		<li><a href="./member/signOut.php">로그아웃[<?=$sid?>님]</a></li>
		<li><a href="./member/member_mypage.php">마이 페이지[<?=$sid?>님]</a></li>
		<?php }else{ ?>
		<li><a href="./member/signUpForm.php">아직 회원이 아니세요?</a></li>
		<li><a href="./main.php">로그인</a></li>
		<?php } ?>
	      </ul>
		
	      </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="blur" src="http://cfile23.uf.tistory.com/image/236DB43555136FB716571A" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>키보드로 악기연주를</h1>
              <p>키보드를 누르면서, 마우스를 클릭하면서 악기를 연주해 보세요!</p>
              <p><a class="btn btn-lg btn-default" href="./member/signUpForm.php" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="blur" src="http://cfile5.uf.tistory.com/image/220D1144543CF95308697A" alt="Second slide"  style="filter:blur(direction=135,strength=100)">
          <div class="container">
            <div class="carousel-caption">
              <h1>나만의 음악 만들기</h1>
              <p>악보 만들기 기능을 통해 나만의 음악을 들어 보세요!</p>
              <p><a class="btn btn-lg btn-default" href="./piano/play3.php" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="blur" src="http://cfs5.tistory.com/upload_control/download.blog?fhandle=YmxvZzEwMDMyOEBmczUudGlzdG9yeS5jb206L2F0dGFjaC8wLzEzMDAwMDAwMDAwMS5qcGc%3D" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>공유하기</h1>
              <p>나만의 음악을 게시판, SNS를 통해 공유하고 추천해 보세요!</p>
              <p><a class="btn btn-lg btn-default" href="./board/list_action.php?page=1" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="./img/piano_icon.png" alt="Generic placeholder image" width="140" height="140">
          <h2>연주하기</h2>
          <p>AdultPiano를 통해 피아노가 없어도 키보드와 마우스로 생동감있는 악기연주를 할 수 있습니다.</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="./img/record_icon.png" alt="Generic placeholder image" width="140" height="140">
          <h2>재생 및 악보 저장하기</h2>
          <p>누구나 쉽게 나만의 음악을 만들고 다시 들을 수 있습니다.</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
          <img class="img-circle" src="./img/share_icon.png" alt="Generic placeholder image" width="140" height="140">
          <h2>공유하기</h2>
          <p>혼자만의 음악이 아닌 우리 모두의 음악이 될 수 있습니다. 게시판 혹은 SNS를 통해 여러분의 음악을 표현해 주세요!</p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

      <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2016 AdultPiano.com by 강다솔,강한올,김종대,박진표</p>
      </footer>

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="./js/vendor/jquery.min.js"><\/script>')</script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="./js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

